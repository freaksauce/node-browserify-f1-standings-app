'user strict';

var _ = require('lodash');

module.exports = {
    f1Sort: function(nestedObj, prop, arr, dir) {
       arr.sort(function (a, b) {
            if (a[nestedObj][prop] < b[nestedObj][prop]) {
                return -1;
            } else if (a[nestedObj][prop] > b[nestedObj][prop]) {
                return 1;
            } else {
                return 0;
            }
        });
        if (dir == 'desc') {
            arr.reverse();   
        }
        return arr;
    },
    searchFilter: function(searchTerm, searchField, arr, dir) {
        var returnObj = {};
        var dataArr = [];
        searchTerm = searchTerm.toLowerCase();

        for(i = 0, len = arr.length; i < len; i++) {
            var points = arr[i].points;
            var position = arr[i].position;
            var driver = arr[i].Driver;
            var constructors = arr[i].Constructors;
            var tempObj = {};
            tempObj.points = points;
            tempObj.position = position;
            tempObj.Driver = driver;
            tempObj.Constructors = constructors;

            for (j in driver) {
                if (driver[j].toLowerCase().indexOf(searchTerm) != -1) {
                    dataArr.push(tempObj);
                }
            }

            for (k in constructors) {
                var c_name = constructors[k].name.toLowerCase();
                if (c_name.indexOf(searchTerm) != -1) {
                    dataArr.push(tempObj);
                }
            }
        }

        dataArr = _.uniq(dataArr);        

        return dataArr;
    }
}