'user strict';

var request = require('superagent');
var EventEmitter = require('events').EventEmitter;
var jsonp = require('jsonp');

module.exports = new EventEmitter();

// add a test to see if the api fails and if so include a snapshot from a text file instead

request.get('http://ergast.com/api/f1/2014/driverStandings.json')
		.end(function(err, res) {
			if (err) {
				console.log(err);
				// get snapshot from hosted url		
				var snapshot_url = 'http://freaksauce.com/node/f1/snapshot.txt';
				var snapshot = jsonp(snapshot_url, {}, function(err, data) {
					if (err) {
						console.log(err);
					}else{
						console.log(data);
					}
				});
				// jsonp
			}else{
				module.exports.emit('ready', res);
			}
		});

