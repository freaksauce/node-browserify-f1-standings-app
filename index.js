'user strict';


var emitter = require('./ergastService');
var _ = require('lodash');
var f1Filter = require('./f1.filter');
var DriverStandings = null;
var MasterDriverStandings = null;

emitter.on('ready', function(res) {
	console.log('emitter is ready');
	//console.log(res.body.MRData);
	initResultsTable();
	buildList(res.body.MRData);
	initFilters();
});

var buildList = function(results) {
	var StandingsTable = results.StandingsTable;
	var StandingsLists = StandingsTable.StandingsLists[0];
	//console.log(StandingsLists);
	var season = StandingsLists.season;
	var round = StandingsLists.round;
	DriverStandings = StandingsLists.DriverStandings;
	MasterDriverStandings = DriverStandings;
	//console.log('Season '+season+' : Round: '+round);
	//console.log(DriverStandings);

	buildResultsTable();
}

/**
	Setup the initial results table ready to inject the api response into from buildResultsTable()
**/
var initResultsTable = function () {
	var _html = '<div id="resultsContainer">';
	_html += '<input type="text" class="search"> <input class="searchBtn" type="button" value="go" />';
	_html += '<table class="resultsTable"><thead>';
	_html += '<tr><td>Constructor</td><td><a href="#" class="givenName" data-dir="asc">First Name</a></td><td><a href="#" data-dir="asc" class="surname">Surname</a></td><td>Position</td><td>Points</td></tr></thead>';

	_html += '</table></div>';
	var div = document.createElement("div");
	document.body.appendChild(div);
	div.innerHTML = _html;
}

/**
	now that we have the driver information from the API, loop and create the table rows to inject into the table
**/
var buildResultsTable = function() {
	var _html = '';
	_.forEach(DriverStandings, function(driver) {
		_html += '<tr><td>'+driver.Constructors[0].name+'</td><td>'+driver.Driver.givenName+'</td><td>'+driver.Driver.familyName+'</td><td>'+driver.position+'</td><td>Points: '+driver.points+'</td></tr>';
	});
	
	var tbody = document.createElement('tbody');
	tbody.innerHTML = _html;
	var table = document.querySelector(".resultsTable");
	table.appendChild(tbody);
}

/**
	setup the filtering and search field
**/
var initFilters = function() {

	var givenName = document.querySelector(".givenName");
	givenName.addEventListener('click', function(e) {
		var currDir = this.getAttribute('data-dir');
		sortResults('Driver', 'givenName', currDir, this);
		e.preventDefault();
	});

	var surname = document.querySelector('.surname');
	surname.addEventListener('click', function(e) {
		var currDir = this.getAttribute('data-dir');
		sortResults('Driver', 'familyName', currDir, this);
		e.preventDefault();
	});

	var searchBtn = document.querySelector('.searchBtn');
	var searchField = document.querySelector('.search');
	searchBtn.addEventListener('click', function(e) {
		var sortedArr = f1Filter.searchFilter(searchField.value, 'givenName', MasterDriverStandings);
		DriverStandings = sortedArr;
		rebuildList();
		e.preventDefault();
	});
}

/**
	sort results table by field and asc/desc
**/
var sortResults = function(sortField, sortBy, dir, elem) {
	console.log('field: '+sortField+' sort:'+sortBy);
	var sortedArr = f1Filter.f1Sort(sortField, sortBy, DriverStandings, dir);
	if (dir === 'asc') {
		dir = 'desc';
	}else{
		dir = 'asc';
	}
	elem.setAttribute('data-dir', dir);
	DriverStandings = sortedArr;
	rebuildList();
}

/**
	remove the current results rows from the table and insert the new results
**/
var rebuildList = function(results) {
	var elem = document.getElementsByTagName('tbody')[0];
	elem.parentNode.removeChild(elem);
	buildResultsTable();
}