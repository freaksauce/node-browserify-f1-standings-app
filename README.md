# README #

This app is a basic experiment using browserify *http://browserify.org/* and is loosely based on the 'First Angular App' tutorial by RAONI BOAVENTURA (*http://www.toptal.com/angular-js/a-step-by-step-guide-to-your-first-angularjs-app*)

I grab the current F1 standings JSON from the Ergast API, build an html table and include some basic A>Z, Z>A name filtering and a search function. I used lodash (currently the full installation but a custom build would reduce the file size dramatically for production code including only the functions I used) and the Node EventEmitter to notify the application when the JSON was ready to process.