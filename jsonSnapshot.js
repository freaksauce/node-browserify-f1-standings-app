'use strict'

var request = require('superagent');
var fs = require('fs');

// write to a text file
request.get('http://ergast.com/api/f1/2014/driverStandings.json')
	.end(function(res) {
		fs.appendFile('tmp/snapshot.txt', JSON.stringify(res.body, null, 4), function(err) {
			if (err) {
				console.log(err);
			}else{
				console.log('The files was saved');
			}
		});
});